# Subito Test

Test per subito.it, l'app consente la ricerca di utenti GitHub e la visualizzazione dei repositories da loro creati.
Al click su un repository si visualizzano gli Stargazer, cioè gli utenti che hanno aggiunto il repository ai preferiti.

![](device-2016-12-30-155939.png) ![](device-2016-12-30-160027.png)

## Utilizzo

All'apertura dell'app inserire il nome dell'utente GitHub che si vuole ricercare, la ricerca avviene in automatico dopo le due lettere e 550 millisecondi.
Se non vengono trovati repository viene mostrato un messaggio di errore, altrimenti viene visualizzata la lista e al click su un elemento vengono scricati gli stargazer del repo.
Al click sul repo si visualizzano i repository dell'utente cliccato.

## Librerie utilizzate

-  [ButterKnife](https://github.com/JakeWharton/butterknife)
-  [FastAdapter](https://github.com/mikepenz/FastAdapter)
-  [FontBinder](https://github.com/nitrico/FontBinder)
-  [Iconics](https://github.com/mikepenz/Android-Iconics)
-  [Picasso](https://github.com/square/picasso)

## Meta

[Andrea Marozzi](http://andreamarozzi.com) – andmaroz89@gmail.com

Distributed under the [WTFPL](http://www.wtfpl.net/) license.
