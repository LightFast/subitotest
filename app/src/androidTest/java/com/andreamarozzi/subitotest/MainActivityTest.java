package com.andreamarozzi.subitotest;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingPolicies;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.text.format.DateUtils;

import com.andreamarozzi.subitotest.features.home.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.andreamarozzi.subitotest.ExtraAssertions.isVisible;

/**
 * Created by Andrea on 30/12/2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    private static final String userToTest = "MikePenz";
    private static final int waitingTime = 5;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void resetTimeout() {
        IdlingPolicies.setMasterPolicyTimeout(60, TimeUnit.SECONDS);
        IdlingPolicies.setIdlingResourceTimeout(26, TimeUnit.SECONDS);
    }

    @Test
    public void testSubitoTest() {
        onView(withId(R.id.user_edit_text))
                .perform(typeText(userToTest), closeSoftKeyboard());

        onView(withId(R.id.user_edit_text))
                .check(matches(withText(userToTest)));

        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.SECONDS);
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.SECONDS);

        IdlingResource idlingResource = new ElapsedTimeIdlingResource(DateUtils.SECOND_IN_MILLIS * waitingTime);
        Espresso.registerIdlingResources(idlingResource);

        onView(withId(R.id.repo_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        Espresso.unregisterIdlingResources(idlingResource);

        onView(withId(R.id.fragment_repository_details))
                .check(isVisible());

        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.SECONDS);
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.SECONDS);

        idlingResource = new ElapsedTimeIdlingResource(DateUtils.SECOND_IN_MILLIS * waitingTime);
        Espresso.registerIdlingResources(idlingResource);

        onView(withId(R.id.repo_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        Espresso.unregisterIdlingResources(idlingResource);
    }
}