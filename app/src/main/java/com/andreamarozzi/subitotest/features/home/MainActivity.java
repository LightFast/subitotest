package com.andreamarozzi.subitotest.features.home;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.andreamarozzi.core.model.Repository;
import com.andreamarozzi.core.model.User;
import com.andreamarozzi.subitotest.R;
import com.andreamarozzi.subitotest.features.repository.FindRepositoryFragment;
import com.andreamarozzi.subitotest.features.repository.RepositoryDetailsFragment;
import com.andreamarozzi.subitotest.features.shared.utils.BaseAppActivity;

public class MainActivity extends BaseAppActivity implements FindRepositoryFragment.OnFindRepositoryListener,
        RepositoryDetailsFragment.OnRepositoryDetailsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        setFragment(FindRepositoryFragment.getInstance());
    }

    @Override
    public void onRepositoryClicked(Repository repository) {
        setFragment(RepositoryDetailsFragment.getInstance(repository), true);
    }

    @Override
    public void onUserClick(User user) {
        setFragment(FindRepositoryFragment.getInstance(user.getLogin()), true);
    }
}
