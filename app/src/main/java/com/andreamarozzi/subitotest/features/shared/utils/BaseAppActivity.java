package com.andreamarozzi.subitotest.features.shared.utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.andreamarozzi.subitotest.R;
import com.mikepenz.iconics.context.IconicsContextWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrea on 01/02/2015.
 */
public abstract class BaseAppActivity extends AppCompatActivity {

    private AsyncTask task;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    protected void showShortSnackbar(int message) {
        showShortSnackbar(getString(message), findViewById(android.R.id.content));
    }

    protected void showShortSnackbar(int message, View view) {
        showShortSnackbar(getString(message), view);
    }

    protected void showShortSnackbar(String message) {
        showShortSnackbar(message, findViewById(android.R.id.content));
    }

    protected void showShortSnackbar(String message, View view) {
        setSnackBarTextColor(Snackbar.make(view, message, Snackbar.LENGTH_SHORT)).show();
    }

    protected Snackbar setSnackBarTextColor(Snackbar snackbar) {
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        return snackbar;
    }

    public void setTask(AsyncTask task) {
        this.task = task;
    }

    @Override
    public void onStop() {
        cancelTask();
        super.onStop();
    }

    public void cancelTask() {
        if (task != null && task.getStatus() != AsyncTask.Status.FINISHED)
            task.cancel(true);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void log(String message) {
        Log.d(getClass().getSimpleName(), message);
    }

    protected int getColorApp(int resColor) {
        return ContextCompat.getColor(this, resColor);
    }

    /**
     * Controlla se la lista dei permessin in ingresso è stata concessa, in caso contrario restituisce
     * la lista dei permessi da chiedere
     *
     * @param permission permissi da controllare
     * @return la lista dei permessi da chiedere
     */
    @NonNull
    public final String[] checkPermission(String... permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> pNotGranted = new ArrayList<String>();
            for (int i = 0; i < permission.length; i++) {
                if (!isPermissionGranted(permission[i]))
                    pNotGranted.add(permission[i]);
            }
            return pNotGranted.toArray(new String[pNotGranted.size()]);
        }

        return new String[0];
    }

    /**
     * Controlla se il permesso in ingresso è stato concesso
     *
     * @param permission permesso da controllare.
     *                   Esempio Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS
     * @return true permesso concesso, false in caso contrario
     */
    public final boolean isPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Convenience for calling {@link #setFragment(Fragment, int, boolean)}.
     * con int <code>R.id.container</code> e boolean <code>false</code>
     *
     * @param frag Fragment
     */
    protected void setFragment(Fragment frag) {
        setFragment(frag, R.id.container, false);
    }

    protected void addFragment(Fragment frag) {
        addFragment(frag, R.id.container, false);
    }

    protected void setFragment(Fragment frag, boolean addBack) {
        setFragment(frag, R.id.container, addBack);
    }

    protected void addFragment(Fragment frag, boolean addBack) {
        addFragment(frag, R.id.container, addBack);
    }

    /**
     * Convenience for calling {@link #setFragment(Fragment, int, String, boolean)}.
     * con String data <code>Fragment.getClass().getName();</code>
     *
     * @param frag        Fragment
     * @param idContainer id of the layout
     * @param addBack     true if add to the back stack
     */
    protected void setFragment(Fragment frag, int idContainer, boolean addBack) {
        setFragment(frag, idContainer, frag.getClass().getName(), addBack);
    }

    protected void addFragment(Fragment frag, int idContainer, boolean addBack) {
        addFragment(frag, idContainer, frag.getClass().getName(), addBack);
    }

    /**
     * Se the fragment on the view indicate by the container
     *
     * @param frag      fragment to set
     * @param container view to put the fragment
     * @param tag       tag for identifier the fragment on the activity
     */
    protected void setFragment(Fragment frag, int container, String tag, boolean addBack) {
        if (isFinishing())
            return;

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, frag, tag);
        if (addBack)
            transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();
    }

    protected void addFragment(Fragment frag, int container, String tag, boolean addBack) {
        if (isFinishing())
            return;

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, frag, tag);
        if (addBack)
            transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();
    }

    protected boolean removeFragment(int id) {
        return removeFragment((Fragment) getFragment(id));
    }

    protected boolean removeFragment(Class c) {
        return removeFragment((Fragment) getFragment(c));
    }

    protected boolean removeFragment(String tag) {
        return removeFragment((Fragment) getFragment(tag));
    }

    protected boolean removeFragment(Fragment frag) {
        if (frag == null)
            return false;
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.remove(frag);
        transaction.commitAllowingStateLoss();

        return true;
    }

    protected boolean isFragmentOnScreen(Class tag) {
        return isFragmentOnScreen(tag.getName());
    }

    protected boolean isFragmentOnScreen(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag) != null;
    }

    @SuppressWarnings("unchecked")
    protected <T extends Fragment> T getFragment(int id) {
        Fragment frag = getFragmentManager().findFragmentById(id);
        if (frag != null)
            return (T) frag;

        return null;
    }

    protected <T extends Fragment> T getFragment(Class c) {
        return getFragment(c.getName());
    }

    @SuppressWarnings("unchecked")
    protected <T extends Fragment> T getFragment(String tag) {
        Fragment frag = getFragmentManager().findFragmentByTag(tag);
        if (frag != null)
            return (T) frag;

        return null;
    }

    @Override
    public void onBackPressed() {
        if (getNumItemBackStack() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public int getNumItemBackStack() {
        return getFragmentManager().getBackStackEntryCount();
    }

    public void clearStackFragment() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}