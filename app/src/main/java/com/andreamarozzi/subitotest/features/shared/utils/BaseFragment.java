package com.andreamarozzi.subitotest.features.shared.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by Andrea on 01/02/2015.
 */
public abstract class BaseFragment extends Fragment {

    private Call call;
    private AsyncTask task;

    protected void showShortSnackbar(int message) {
        showShortSnackbar(getString(message), getActivity().findViewById(android.R.id.content));
    }

    protected void showShortSnackbar(int message, View view) {
        showShortSnackbar(getString(message), view);
    }

    protected void showShortSnackbar(String message) {
        showShortSnackbar(message, getActivity().findViewById(android.R.id.content));
    }

    protected void showShortSnackbar(String message, View view) {
        setSnackBarTextColor(Snackbar.make(view, message, Snackbar.LENGTH_SHORT)).show();
    }

    protected Snackbar setSnackBarTextColor(Snackbar snackbar) {
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        return snackbar;
    }

    public void setTask(AsyncTask task) {
        this.task = task;
    }

    public void setCall(Call call) {
        this.call = call;
    }

    @Override
    public void onStop() {
        cancelTask();
        cancelCall();
        super.onStop();
    }

    public void cancelTask() {
        if (task != null && task.getStatus() != AsyncTask.Status.FINISHED)
            task.cancel(true);
    }

    public void cancelCall() {
        if (call != null)
            call.cancel();
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void log(String message) {
        Log.d(getClass().getSimpleName(), message);
    }

    protected int getColorApp(int resColor) {
        return ContextCompat.getColor(getActivity(), resColor);
    }

    /**
     * Controlla se la lista dei permessin in ingresso è stata concessa, in caso contrario restituisce
     * la lista dei permessi da chiedere
     *
     * @param permission permissi da controllare
     * @return la lista dei permessi da chiedere
     */
    @NonNull
    public final String[] checkPermission(String... permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> pNotGranted = new ArrayList<String>();
            for (int i = 0; i < permission.length; i++) {
                if (!isPermissionGranted(permission[i]))
                    pNotGranted.add(permission[i]);
            }
            return pNotGranted.toArray(new String[pNotGranted.size()]);
        }

        return new String[0];
    }

    /**
     * Controlla se il permesso in ingresso è stato concesso
     *
     * @param permission permesso da controllare.
     *                   Esempio Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS
     * @return true permesso concesso, false in caso contrario
     */
    @SuppressLint("NewApi")
    public final boolean isPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }
}