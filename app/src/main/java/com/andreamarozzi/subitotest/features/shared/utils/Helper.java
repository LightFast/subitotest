package com.andreamarozzi.subitotest.features.shared.utils;

import android.graphics.Typeface;

import com.github.nitrico.fontbinder.FontBinder;

/**
 * Created by Andrea
 */

public class Helper {

    public static Typeface getFontTitle() {
        return FontBinder.get("rounded");
    }

    public static Typeface getFontContent() {
        //return FontBinder.get("book");
        return FontBinder.get("neue");
    }
}
