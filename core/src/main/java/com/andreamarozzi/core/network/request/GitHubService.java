package com.andreamarozzi.core.network.request;

import com.andreamarozzi.core.model.Repository;
import com.andreamarozzi.core.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Andrea on 29/12/2016.
 */

public interface GitHubService {

    @GET("users/{user}/repos")
    Call<List<Repository>> getListUserRepositories(@Path("user") String user);

    @GET("repos/{user}/{repository}/stargazers")
    Call<List<User>> requestRepositoryStargazers(@Path("user") String userName, @Path("repository") String repositoryName);
}